' +---------------------------------------------------------------------------+
' | Skoobalon Roku Library                                                    |
' | Copyright (C) 2014 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+

'
' Example use of the ImprovedSlideshow component
' (See the ShowImprovedSlideshow function.)
'
Sub Main()
	' Present the user with a menu where they can choose which slideshow to view
	port = CreateObject("roMessagePort")
	screen = CreateObject("roListScreen")
	screen.SetMessagePort(port)
	screen.SetTitle("ImprovedSlideshow Demo")
	screen.SetContent([
		{Title: "Show ImprovedSlideshow"}, 
		{Title: "Show roSlideShow"}
	])
	screen.Show()
	
	While True
		msg = wait(0, port)
		If Type(msg) = "roListScreenEvent"
			If msg.IsScreenClosed()
				Exit While
			Else If msg.IsListItemSelected()
				If msg.GetIndex() = 0 Then ShowImprovedSlideshow()
				If msg.GetIndex() = 1 Then ShowRoSlideShow()
			End If
		End If
	End While
End Sub ' Main

Function GetContentList() as Object
	Return [
		{
			Url: "http://www.skoobalon.com/music/songs/Lander_The_Original_Soundtrack/cover-480.jpg"
			TextOverlayUL: "ImprovedSlideshow Example Channel"
			TextOverlayUR: "1 of 3"
			TextOverlayBody: "Album cover for Lander: The Original Soundtrack." + Chr(10) + "Visit http://www.skoobalon.com/music for more details."
		},
		{
			Url: "http://www.skoobalon.com/music/songs/Hit_The_Wall/cover-480.jpg"
			TextOverlayUL: "ImprovedSlideshow Example Channel"
			TextOverlayUR: "2 of 3"
			TextOverlayBody: "Another classic Isaac Jacobs & The Abrahams album: Hit The Wall."
		},
		{
			Url: "http://www.skoobalon.com/music/songs/Drone_EP/cover-480.jpg"
			TextOverlayUL: "ImprovedSlideshow Example Channel"
			TextOverlayUR: "3 of 3"
			TextOverlayBody: "The Drone EP. From the earlier days when the band was still calling itself Loaded."
		}
	]
End Function ' GetContentList

Function ShowRoSlideShow() as Void
	' Create an roSlideShow object with some sample images
	port = CreateObject("roMessagePort")
	slideshow = CreateObject("roSlideShow")
	slideshow.SetMessagePort(port)
	slideshow.SetContentList(GetContentList())
	
	slideshow.Show()
	
	' Process events
	While True
		msg = wait(0, port)
		If Type(msg) = "roSlideShowEvent" And msg.IsScreenClosed() Then Exit While
	End While
End Function ' ShowRoSlideShow

Function ShowImprovedSlideshow() as Void
	' Create an ImprovedSlideshow object with some sample images
	slideshow = NewImprovedSlideshow()
	slideshow.SetContentList(GetContentList())
	port = slideshow.GetMessagePort()
	
	slideshow.Show()
	
	While Not slideshow.IsClosed()
		' Process any input. Here you can override the default behavior for 
		' messages by not forwarding them on to the component.
		' You will see roUrlEvent and roUniversalControlEvents here, for file 
		' downloads and user input, respectively.
		msg = port.GetMessage()
		While msg <> Invalid
			slideshow.HandleMessage(msg)
			msg = port.GetMessage()
		End While
		
		' Updates all logic for a frame of the slideshow
		slideshow.UpdateLogic()
		
		' Draws the next frame of the slideshow
		slideshow.Draw()
	End While
End Function ' ShowImprovedSlideshow
