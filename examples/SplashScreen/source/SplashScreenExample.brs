' +---------------------------------------------------------------------------+
' | Skoobalon Roku Library                                                    |
' | Copyright (C) 2014 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+


'
' Example demonstrating the proper use of the SplashScreen component
'
Sub Main()
	' Create a placeholder screen. This prevents the program from exiting
	' at the end of the splash screen function when the last remaining
	' screen is invalidated.
	placeholder = CreateObject("roScreen")

	' Show the splash screen
	ShowSplashScreen("pkg:/images/SplashScreen.jpg", 1.0, 1.0)
	
	? "Exiting program"
End Sub ' Main
