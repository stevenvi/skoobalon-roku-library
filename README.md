# README #

The Skoobalon Roku Library is a collection of common components for Roku applications. All components have been tested with a Roku 3 in HD mode. It is likely that they will also function on other Roku models, though their performance is unknown at this time. SD mode may require some code tweaks. Please send a pull request if you have improvements!

### License ###

This software is licensed under the GNU General Public License v2. See the gpl-2.0.txt file in the repository for more details. 

### Getting Started ###

1. First, you'll need a [Roku](http://www.roku.com) and a [Developer Account](https://www.roku.com/developer). Since you're reading this document, it's likely that you already have these set up.
2. Set your Roku in [developer mode](http://sdkdocs.roku.com/display/sdkdoc/Developer+Guide#DeveloperGuide-52ApplicationSecurity) and create a password for it.
3. While not required, it is recommended that you use a Linux box for development.
4. Next, you'll want to have a thorough understanding of [BrightScript and the Roku SDK](http://sdkdocs.roku.com/display/sdkdoc/Roku+SDK+Documentation). Being an expert with the language is critical to proper development.
5. In your Linux shell, add the following properties:
```
#!bash
export ROKU_DEV_TARGET=<IP of your Roku>
export DEVPASSWORD=<Your Roku's developer password>
```
Try out some of the example channels by navigating to the examples directory, entering the subdirectory for the example of interest, and typing 
```
#!bash
make install
```

To use these components in your own channel, include them in your channel's packaging and use them. That's all there is to it!

# Documentation #

### DrawUtil ###
The DrawUtil singleton contains helper methods for drawing to roBitmap objects. This object is obtained by invoking the GetDrawUtil() function.

#### RectFilled(bmp as Object, x1%, y1%, x2%, y2%, color%) as Void ####
Draw a filled rectangle to the destination roBitmap object with one corner at (x1%, y1%) and the opposing corner at (x2%, y2%), using the color provided.

#### RectOutline(bmp as Object, x1%, y1%, x2%, y2%, color%) as Void ####
Draws the outline of a rectangle to the destination roBitmap object with one corner at (x1%, y1%) and the opposing corner at (x2%, y2%), using the color provided.

#### TextWithShadow(bmp as Object, text$, x%, y%, font as Object, textColor% = &hFFFFFFFF, shadowColor% = &hFF) as Void ####
Draws the input text$ in the font provided to the destination bitmap, first in the shadowColor%, then again in the textColor%. Presently the shadow is always offset by 2 pixels down and to the right.


### EaseUtil ###
The EaseUtil singleton contains predefined easing functions which can be used with the Tweener class. This object is obtained by invoking the GetEaseUtil() function. A thorough description of all the easing functions will not be provided, as the name should be self-explanitory and they are much better experienced rather than described. A good resource on easing functions is [http://gizma.com/easing/](http://gizma.com/easing/).

#### EaseLinear ####
#### EaseQuadIn ####
#### EaseQuadOut ####
#### EaseQuadInOut ####
#### EaseCubicInOut ####


### Math ###
The Math singleton contains helper methods for performing mathematical operations. This object is obtained by invoking the GetMath() function.

#### Min(x, y) ####
Returns the smaller of the two input values.

#### Max(x, y) ####
Returns the larger of the two input values.


### SimpleDialog ###
A simple dialog box using the roMessageDialog component. A blocking dialog can be created with this object. A new instance of this object is created with the NewSimpleDialog(title$, text$, showBusyAnimation as Boolean, buttons as Object) function.

* title$ The title to use for the dialog
* text$ The text to use for the body of the dialog
* showBusyAnimation Flags if the busy animation should be displayed in this box
* buttons An roArray of strings with the values to use for each button on this dialog

#### Show() as Integer ####
Shows the dialog box and blocks program execution until the user has made a selection of one of the provided buttons. Returns the index of the button that was selected by the user.