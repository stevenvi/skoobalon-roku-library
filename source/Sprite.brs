' +---------------------------------------------------------------------------+
' | Skoobalon Roku Library                                                    |
' | Copyright (C) 2014 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+


Function NewSpriteFromFile(file$, x, y, alpha, scaleX, scaleY) as Object
	bmp = CreateObject("roBitmap", file$)
	? "Loading "; file$; ": "; bmp
	Return NewSprite(bmp, x, y, alpha, scaleX, scaleY)
End Function ' NewSpriteFromFile

Function NewSprite(bmp, x, y, alpha, scaleX, scaleY) as Object
	obj = {
		_region: Invalid
		_x!: x
		_y!: y
		_alpha!: alpha
		_scaleX!: scaleX
		_scaleY!: scaleY
	}
	
	' Enable smooth scaling
	If Type(bmp) = "roRegion"
		obj._region = bmp
	Else If Type(bmp) = "roBitmap"
		obj._region = CreateObject("roRegion", bmp, 0, 0, bmp.GetWidth(), bmp.GetHeight())
	Else
		? "Invalid bitmap specified"
		Return Invalid
	End If
	obj._region.SetScaleMode(1)
	
	obj.GetX = Function() as Double
		Return m._x!
	End Function ' GetX
	
	obj.SetX = Function(x!) as Void
		m._x! = x!
	End Function ' SetX
	
	obj.GetY = Function() as Double
		Return m._y!
	End Function ' GetY
	
	obj.SetY = Function(y!) as Void
		m._y! = y!
	End Function ' SetY
	
	obj.GetAlpha = Function() as Double
		Return m._alpha!
	End Function ' GetAlpha
	
	obj.SetAlpha = Function(alpha!) as Void
		' Clamp to [0.0, 1.0]
		Math = GetMath()
		m._alpha! = Math.Max(Math.Min(1.0, alpha!), 0.0)
	End Function ' SetX
	
	obj.GetScaleX = Function() as Double
		Return m._scaleX!
	End Function ' GetScaleX
	
	obj.SetScaleX = Function(scaleX!) as Void
		m._scaleX! = scaleX!
	End Function ' SetScaleX
	
	obj.GetScaleY = Function() as Double
		Return m._scaleY!
	End Function ' GetScaleY
	
	obj.SetScaleY = Function(scaleY!) as Void
		m._scaleY! = scaleY!
	End Function ' SetScaleY
	
	obj.GetRegion = Function() as Object
		Return m._region
	End Function ' GetRegion
	
	obj.Draw = Function(screen as Object) as Void
		If screen <> Invalid
			alpha% = Int(255.0 * m._alpha!)
			If m.GetScaleX() <> 1.0 Or m.GetScaleY() <> 1.0
				' Scale
				If alpha% <> 255
					' With alpha
					screen.DrawScaledObject(Int(m._x!), Int(m._y!), m._scaleX!, m._scaleY!, m._region, &hFFFFFF00 + alpha%)
				Else
					' Without alpha
					screen.DrawScaledObject(Int(m._x!), Int(m._y!), m._scaleX!, m._scaleY!, m._region)
				End If
			Else
				' Do not scale
				If alpha% <> 255
					' With alpha
					screen.DrawObject(Int(m._x!), Int(m._y!), m._region, &hFFFFFF00 + alpha%)
				Else
					' Without alpha
					screen.DrawObject(Int(m._x!), Int(m._y!), m._region)
				End If
			End If
		End If
	End Function ' Draw
	
	Return obj
End Function ' NewSprite