' +---------------------------------------------------------------------------+
' | Skoobalon Roku Library                                                    |
' | Copyright (C) 2014 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+


'
' Singleton
' Utility for drawing onto a bitmap
'
Function GetDrawUtil() as Object
	globals = GetGlobalAA()
	If globals._DrawUtilInstance = Invalid Then globals._DrawUtilInstance = _NewDrawUtil()
	Return globals._DrawUtilInstance
End Function ' GetDrawUtil

Function _NewDrawUtil() as Object
	obj = {}
	
	obj.RectFilled = Function(bmp as Object, x1%, y1%, x2%, y2%, color%) as Void
		bmp.DrawRect(x1%, y1%, x2% - x1%, y2% - y1%, color%)
	End Function ' FilledRect
	
	obj.RectOutline = Function(bmp as Object, x1%, y1%, x2%, y2%, color%) as Void
		bmp.DrawLine(x1%, y1%, x1%, y2%, color%)
		bmp.DrawLine(x1%, y2%, x2%, y2%, color%)
		bmp.DrawLine(x2%, y2%, x2%, y1%, color%)
		bmp.DrawLine(x2%, y1%, x1%, y1%, color%)
	End Function ' FilledRect
	
	obj.TextWithShadow = Function(bmp as Object, text$, x%, y%, font as Object, textColor% = &hFFFFFFFF, shadowColor% = &hFF) as Void
		bmp.DrawText(text$, x% + 2, y% + 2, shadowColor%, font)
		bmp.DrawText(text$, x%, y%, textColor%, font)
	End Function ' TextWithShadow
	
	Return obj
End Function ' _NewDrawUtil