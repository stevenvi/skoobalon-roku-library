' +---------------------------------------------------------------------------+
' | Skoobalon Roku Library                                                    |
' | Copyright (C) 2014 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+


Function NewTweener(sprite as Object, easingFunction as Function) as Object
	' Populate default values
	obj = {
		_sprite: sprite			' The Sprite object to be acted upon
		_duration!: 1.0			' Duration of the tween, in seconds
		_easingFunction: easingFunction ' Easing function to use, should take (b, c, t, d) as arguments (see EaseUtil.brs)
		_hasStarted: false		' Flags if the tween has been started yet
		_timeElapsed!: 0.0		' Total time elapsed on this tween
	}
	
	obj.Duration = Function(timeInSeconds!) as Object
		m._duration! = timeInSeconds!
		Return m
	End Function ' Duration
	
	obj.Alpha = Function(alpha0!, alpha1!) as Object
		m._alphaB! = alpha0!
		m._alphaC! = alpha1! - alpha0!
		Return m
	End Function ' Alpha
	
	obj.X = Function(x0!, x1!) as Object
		m._xB! = x0!
		m._xC! = x1! - x0!
		Return m
	End Function ' X
	
	obj.Y = Function(y0!, y1!) as Object
		m._yB! = y0!
		m._yC! = y1! - y0!
		Return m
	End Function ' Y
	
	obj.ScaleX = Function(scaleX0!, scaleX1!) as Object
		m._scaleXB! = scaleX0!
		m._scaleXC! = scaleX1! - scaleX0!
		Return m
	End Function ' ScaleX
	
	obj.ScaleY = Function(scaleY0!, scaleY1!) as Object
		m._scaleYB! = scaleY0!
		m._scaleYC! = scaleY1! - scaleY0!
		Return m
	End Function ' ScaleY
	
	' Starts tweening
	obj.Start = Function() as Object
		If Not m._hasStarted
			data = _TweenerGetGlobalData()
			data._activeTweens.Push(m)
			m._hasStarted = True
			m._firstFrame = True
			m._Tick(0.0)
		End If
		Return m
	End Function
	
	' Recomputes the position for the tween at the current point in time
	obj._Tick = Function(dt!) as Void
		m._timeElapsed! = m._timeElapsed! + dt!
		If m._timeElapsed! > m._duration! 
			m._timeElapsed! = m._duration!
		End If
		
		If m._alphaB!  <> Invalid Then m._sprite.SetAlpha( m._easingFunction(m._alphaB!, m._alphaC!, m._timeElapsed!, m._duration!))
		If m._xB!      <> Invalid Then m._sprite.SetX(     m._easingFunction(m._xB!, m._xC!, m._timeElapsed!, m._duration!))
		If m._yB!      <> Invalid Then m._sprite.SetY(     m._easingFunction(m._yB!, m._yC!, m._timeElapsed!, m._duration!))
		If m._scaleXB! <> Invalid Then m._sprite.SetScaleX(m._easingFunction(m._scaleXB!, m._scaleXC!, m._timeElapsed!, m._duration!))
		If m._scaleYB! <> Invalid Then m._sprite.SetScaleY(m._easingFunction(m._scaleYB!, m._scaleYC!, m._timeElapsed!, m._duration!))
	End Function
	
	' Returns true if the tween is complete, false otherwise
	obj.IsDone = Function() as Boolean
		Return m._timeElapsed! >= m._duration!
	End Function
	
	Return obj
End Function

' Must be called in your main logic loop to update tweens
Function TweenerUpdateTweens()
	data = _TweenerGetGlobalData()
	dt! = data._timespan.TotalMilliseconds() / 1000.0
	data._timespan.Mark()
	
	' A For Each doesn't handle removals nicely, so we're doing this a little more manually
	i% = 0
	While True
		' Escape condition: once we've iterated over the entire list
		If i% >= data._activeTweens.Count() Then Exit While
		
		tween = data._activeTweens[i%]
		If tween._firstFrame
			tween._firstFrame = false
			tween._Tick(0.0)
		Else
			tween._Tick(dt!)
		End If
		
		If tween.IsDone()
			data._activeTweens.Delete(i%)
		Else
			' Increment index
			i% = i% + 1
		End If
	End While
End Function ' UpdateTweens

Function TweenerIsTweening(sprite as Object) as Boolean
	activeTweens = _TweenerGetGlobalData()._activeTweens
	For Each tween in activeTweens
		If tween._sprite._id = sprite._id Then Return True
	End For
	Return False
End Function

' Use a single tween timer to save on resources and processing
' This method returns the global tween time, in milliseconds
Function _TweenerGetGlobalData() as Object
	globals = GetGlobalAA()
	If globals._TweenerData = Invalid
		globals._TweenerData = {
			_timespan: CreateObject("roTimespan")
			_activeTweens: []
		}
	End If
	Return globals._TweenerData
End Function
