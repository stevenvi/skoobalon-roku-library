' +---------------------------------------------------------------------------+
' | Skoobalon Roku Library                                                    |
' | Copyright (C) 2014 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+


Function GetTextUtil() as Object
	globals = GetGlobalAA()
	If globals._TextUtilInstance = Invalid Then globals._TextUtilInstance = _NewTextUtil()
	Return globals._TextUtilInstance
End Function ' TextUtil

Function _NewTextUtil() as Object
	obj = {}
	
	' Returns an roArray with each line of the wrapped text
	' This is a little naive: it strips any duplicated whitespace and only uses spaces
	' TODO: Can we speed this up with a binary search??
	obj.Wrap = Function(text$, font as Object, lineWidth%) as Object
		' Sanity check on input
		em% = font.GetOneLineWidth("M", &hFFFFFFFF)
		If lineWidth% <= em% Then Return [text$]
		
		? "Tokenizing '" + text$ + "':"
		
		lines = []
		tokens = text$.Tokenize(" ")
		tokenIdx% = 0
		While tokenIdx% < tokens.Count()
			str$ = tokens[tokenIdx%]
			oldStr$ = ""
			While font.GetOneLineWidth(str$, lineWidth% + 1) < lineWidth%
				tokenIdx% = tokenIdx% + 1
				If tokenIdx% >= tokens.Count()
					? "No more tokens"
					Exit While
				End If
				oldStr$ = str$
				str$ = str$ + " " + tokens[tokenIdx%]
			End While
			
			If oldStr$.Len() = 0 Or tokenIdx% >= tokens.Count()
				' If a single token is longer than the line can be, it'll have to overflow
				' TODO: We could handle this a bit cleaner by splitting it up in this block, but that's extra work not needed for this project
				lines.Push(str$)
			Else
				lines.Push(oldStr$)
			End If
		End While
		
		Return lines
	End Function
	
	Return obj
End Function ' _NewTextUtil
