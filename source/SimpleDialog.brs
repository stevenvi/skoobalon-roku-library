' +---------------------------------------------------------------------------+
' | Skoobalon Roku Library                                                    |
' | Copyright (C) 2014 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+


'
' A very simple blocking dialog box using roMessageDialog
' title$ The title to use for the dialog
' text$ The text to use for the body of the dialog
' showBusyAnimation Flags if the busy animation should be displayed in this box
' buttons An roArray of strings with the values to use for each button on this dialog
'
Function NewSimpleDialog(title$, text$, showBusyAnimation as Boolean, buttons as Object) as Object
	? "SimpleDialog constructor"
	
	obj = {
		_buttonPressed%: -1
	}
	
	' 
	obj.port = CreateObject("roMessagePort")
	obj.dialog = CreateObject("roMessageDialog")
	obj.dialog.SetMessagePort(obj.port)
	obj.dialog.SetTitle(title$)
	obj.dialog.SetText(text$)
	If showBusyAnimation
		obj.dialog.ShowBusyAnimation()
	End If
	
	' Add all buttons to the dialog
	For i% = 0 TO buttons.Count() - 1
		obj.dialog.AddButton(i%, buttons[i%])
	End For
	
	' Shows the dialog and blocks program execution until it is dismissed
	' Returns the index of the button that was used to dismiss the dialog box
	obj.Show = Function() as Integer
		m.dialog.Show()
		While True
			msg = wait(0, m.port)
			If Type(msg) = "roMessageDialogEvent"
				If msg.IsScreenClosed()
					Exit While
				Else If msg.IsButtonPressed()
					m._buttonPressed% = msg.GetIndex()
					m.dialog.Close()
				End If
			End If
		End While
		Return m._buttonPressed%
	End Function ' Show
	
	Return obj
End Function
