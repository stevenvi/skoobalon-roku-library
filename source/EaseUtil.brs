' +---------------------------------------------------------------------------+
' | Skoobalon Roku Library                                                    |
' | Copyright (C) 2014 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+

'
' Singleton
' Contains predefined easing functions, courtesy of http://gizma.com/easing/
' These can be used with the Tweener class to define tweens. See the link above
' to learn more about easing functions.
'
Function GetEaseUtil()
	globals = GetGlobalAA()
	If globals._EaseUtilInstance = Invalid Then globals._EaseUtilInstance = _NewEaseUtil()
	Return globals._EaseUtilInstance
End Function ' GetEaseUtil

Function _NewEaseUtil()
	obj = {}
	
	obj.Linear = Function(b, c, t, d)
		Return c * t / d + b
	End Function ' Linear
	
	' 1 - |1 - 2x|
	obj.LinearReturn = Function(b, c, t, d)
		Return c * (1 - Abs(1 - 2 * t / d)) + b
	End Function ' LinearReturn
	
	obj.QuadIn = Function(b, c, t, d)
		t = t / d
		Return c * t * t + b
	End Function ' QuadIn
	
	obj.QuadOut = Function(b, c, t, d)
		t = t / d
		Return -c * t * (t - 2) + b
	End Function ' QuadOut
	
	obj.QuadInOut = Function(b, c, t, d)
		t = t / (d / 2)
		If t < 1 Then Return c / 2 * t * t + b
		t = t - 1
		Return -c / 2 * (t * (t - 2) - 1) + b
	End Function ' QuadInOut
	
	obj.CubicInOut = Function(b, c, t, d)
		t = t / (d / 2)
		If t < 1 Then Return c / 2 * t * t * t + b
		t = t - 2
		Return c / 2 * (t * t * t + 2) + b
	End Function ' CubicInOut
	
	' Hits finish value at halfway through, then returns to original value
	' f(x) = 1 - (2x - 1)^2
	obj.QuadInOutReturn = Function(b, c, t, d)
		t = 2 * t / d - 1
		Return c * (1 - t * t) + b
	End Function ' QuadInOutReturn
	
	obj.CubicInOutReturn = Function(b, c, t, d)
		t = t / d
		If t < 0.5
			t = 1 - 2 * t / d
		Else 
			t = 2 * t / d - 1
		End If
		Return c * (1 - t * t * t) + b
	End Function ' CubicInOutReturn
	
	' f(x) = sqrt(1 - (2x-1)^2)
	obj.CircularInOutReturn = Function(b, c, t, d)
		t = t / d
		x = 2 * t - 1
		Return c * Sqr(1 - x * x) + b
	End Function ' CircularInOutReturn
	
	Return obj
End Function ' _NewEaseUtil