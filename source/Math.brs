' +---------------------------------------------------------------------------+
' | Skoobalon Roku Library                                                    |
' | Copyright (C) 2014 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+


'
' Singleton
' A utility class containing methods for common mathematical operations
'
Function GetMath()
	globals = GetGlobalAA()
	If globals._MathInstance = Invalid Then globals._MathInstance = _NewMath()
	Return globals._MathInstance
End Function ' GetMath()

Function _NewMath()
	obj = {}
	
	' Returns the smaller of the two input values
	obj.Min = Function(x, y)
		If x < y Then Return x
		Return y
	End Function ' Min
	
	' Returns the larger of the two input values
	obj.Max = Function(x, y)
		If x > y Then Return x
		Return y
	End Function ' Max
	
	' Clamps a value between two values, inclusive
	obj.Clamp = Function(value, low, high)
		If value > high Then Return high
		If value < low Then Return low
		Return value
	End Function ' Clamp
	
	Return obj
End Function ' _NewMath()
