' +---------------------------------------------------------------------------+
' | Skoobalon Roku Library                                                    |
' | Copyright (C) 2014 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+

'
' An improved slideshow component which can animate slides in and out in a more
' visually appealing manner as compared to the roSlideshow component, which just
' flicks immediately to the next image.
'
' This is a standalone component which you can reuse in other projects
' Dependencies:
' * EaseUtil.brs
' * Math.brs
' * Sprite.brs
' * Tweener.brs
'
' See examples/ImprovedSlideshowExample/ for a sample channel using this component
'
' Note that the present real-world use for this component is a channel that only
' fetches images hosted on a LAN, which effectively are downloaded instantly.
' If your use varies, you may need to tweak the way that images are downloaded.
' Feel free to send a pull request with your improvements when you're done!
'

'
' Creates a new instance of the ImprovedSlideshow component
'
Function NewImprovedSlideshow() as Object
	obj = {
		' The screen that the slideshow is displayed on
		_screen: Invalid
		
		' Message port
		_port: CreateObject("roMessagePort")
		
		' Flags if this screen is done being shown
		_done: true
		
		' Flags if the text overlay is currently visible
		_showTextOverlay: false
		
		' Flags if the slideshow is currently playing
		_playing: false
		
		' The content for the slideshow. Should be an roArray of roAssociativeArray objects
		' with Content Meta-Data objects for each slide in the slideshow.
		_content: []
		
		' Index of the currently displayed slide
		_currentSlideIndex%: 0
		
		' The currently visible slide
		_currentSlideSprite: Invalid
		_previousSlideQueue: []
		_currentDefaultPosition: Invalid
		_currentZoomFactor!: 1.0
		
		' Play/Pause overlay sprites
		' These are not provided by this library and must instead be provided by your channel
		' If the images do not exist, crude primitives will be used instead.
		_playSprite: NewSpriteFromFile("pkg:/images/icons/Play.png", 0, 0, 0.0, 1.0, 1.0)
		_pauseSprite: NewSpriteFromFile("pkg:/images/icons/Pause.png", 0, 0, 0.0, 1.0, 1.0)
		
		' Time to display each slide, in seconds
		_period!: 5
		_timespan: CreateObject("roTimespan")
		
		' roTimespan that times a short grace period before beginning the tween of the next image
		' This prevents stalling when skipping multiple images at once by repeatedly pressing
		' the button on the remote.
		_showNextImageGracePeriod: Invalid
		
		' Direction of the next slide to show: -1 for moving to the left, +1 for moving to the right
		' This is used in conjunction with the grace period timer to determine how to tween in the image
		_showNextImageDirection%: 0
		
		' Display mode for how slides are scaled. Valid options are:
		' * "scale-to-fill": Scales the image to fill the screen, neglecting aspect ratio
		' * "scale-to-fit": Scales the image to fit on the screen, maintaining aspect ratio without croppping
		' * "zoom-to-fill": Scales the image to fill the screen, maintaining aspect ratio and cropping if necessary
		_displayMode$: "scale-to-fit"
		
		' Headers to add to all roUrlTransfer objects
		_httpHeaders: {}
		
		' roUrlTransfer objects for cached transfers, indexed by transfer identifier and image id
		_cacheUrlTransferById: {}
		_cacheUrlTransferByIdx: {}
		_fetchAll: false
		
		' FileSystem object for performing filesystem operations
		_fs: CreateObject("roFileSystem")

		' The font to use for overlay displays
		_font: CreateObject("roFontRegistry").GetDefaultFont(26, false, false)
	}
	
	' Verify we have a play and pause sprite, or make our own otherwise
	If obj._pauseSprite = Invalid
		spr = CreateObject("roBitmap", {width:100, height:100, AlphaEnable:true})
		spr.Clear(0)
		spr.DrawRect(0, 0, 40, 100, &hFFFFFFFF)
		spr.DrawRect(60, 0, 100, 100, &hFFFFFFFF)
		obj._pauseSprite = NewSprite(spr, 0, 0, 0, 1.0, 1.0)
	End If
	
	If obj._playSprite = Invalid
		' TODO: Fill in this icon. Need to make a filled polygon method...
		spr = CreateObject("roBitmap", {width:100, height:100, AlphaEnable:true})
		spr.Clear(0)
		For i% = 0 To 100
			spr.DrawLine(i%, i%/2, i%, 100-i%/2, &hFFFFFFFF)
		End For
		obj._playSprite = NewSprite(spr, 0, 0, 0, 1.0, 1.0)
	End If
	
	' Returns the roMessagePort used by this component
	obj.GetMessagePort = Function() as Object
		return m._port
	End Function ' GetMessagePort
	
	' Sets the roArray of Content Meta-Data objects for the slides in this slideshow
	' contentList: roArray of Content Meta-Data objects, one for each slide
	' fetchAll: flags if all slides should be fetched immediately
	obj.SetContentList = Function(contentList as Object, fetchAll = false as Boolean) as Void
		m._content = contentList
		m._fetchAll = fetchAll
	End Function ' SetContentList
	
	' Appends an item to the end of the slideshow
	obj.AddContent = Function(contentItem as Object) as Void
		m._content.Push(contentItem)
	End Function ' AddContent
	
	' Removes all items from the slideshow
	obj.ClearContent = Function() as Void
		m._content = []
		m._cacheUrlTransferById = {}
		m._cacheUrlTransferByIdx = {}
		m.Pause()
	End Function ' ClearContent
	
	' Returns the roFont object used on the overlay
	obj.GetFont = Function() as Object
		return m._font
	End Function ' GetFont
	
	' Sets the font to use on the overlay
	' font: roFont to be used
	obj.SetFont = Function(font as Object) as Void
		If font <> Invalid Then m._font = font
	End Function ' SetFont
	
	' Returns the index of the currently visible slide
	obj.GetCurrentSlide = Function() as Integer
		return m._currentSlideIndex%
	End Function ' GetCurrentSlide
	
	' Sets the index of the currently visible slide and tweens to it immediately
	' idx: Index of the slide to be shown
	obj.SetCurrentSlide = Function(idx%) as Void
		' Set to the last slide if it's larger than our total content size
		m._currentSlideIndex% = m._GetPreviousSlideIndex(idx% - 1)
		m._ShowNextSlide()
	End Function ' SetCurrentSlide
	
	' Gets the time between slides, in seconds
	obj.GetPeriod = Function() as Float
		return m._period!
	End Function ' GetPeriod
	
	' Sets the time between slides
	obj.SetPeriod = Function(seconds!) as Void
		m._period! = seconds!
	End Function ' SetPeriod
	
	' Pauses the playback of the slideshow
	' showPauseIcon: Flags if the pause icon should be displayed 
	' Returns true if the pause operation succeeded
	obj.Pause = Function(showPauseIcon = true as Boolean) as Boolean
		If m._playing
			Print "Paused"
			m._playing = False
			If showPauseIcon Then m._PlayPauseTween(m._pauseSprite)
		End If
		Return Not m._playing
	End Function ' Pause
	
	' Resumes playback of the slideshow
	' showPlayIcon: Flags if the play icon should be displayed
	' Returns true if the resume operation succeeded
	obj.Resume = Function(showPlayIcon = true as Boolean) as Boolean
		If Not m._playing
			Print "Resumed"
			m._playing = True
			m._timespan.Mark()
			If showPlayIcon Then m._PlayPauseTween(m._playSprite)
		End If
		Return m._playing
	End Function ' Resume
	
	' Tweens in a sprite overlay similar to how YouTube does it for the play and pause icons
	obj._PlayPauseTween = Function(sprite as Object) as Void 
		' Start as 1/4 of screen height
		' Finish as 1/3 of screen height
		sy0! = m._screen.GetHeight() / 4 / sprite.GetRegion().GetHeight()
		sy1! = m._screen.GetHeight() / 3 / sprite.GetRegion().GetHeight()
		sx0! = sy0!
		sx1! = sy1!
		x0! = (m._screen.GetWidth() - sprite.GetRegion().GetWidth() * sx0!) / 2
		x1! = (m._screen.GetWidth() - sprite.GetRegion().GetWidth() * sx1!) / 2
		y0! = (m._screen.GetHeight()- sprite.GetRegion().GetHeight()* sy0!) / 2
		y1! = (m._screen.GetHeight()- sprite.GetRegion().GetHeight()* sy1!) / 2
		NewTweener(sprite, GetEaseUtil().LinearReturn).Duration(0.6).Alpha(0.0, 0.8).Start()
		NewTweener(sprite, GetEaseUtil().QuadOut).Duration(0.6).ScaleX(sx0!, sx1!).ScaleY(sy0!, sy1!).X(x0!, x1!).Y(y0!, y1!).Start()
	End Function ' _PlayPauseTween
	
	' 
	obj.NextImage = Function() as Void
		m._showNextImageGracePeriod = CreateObject("roTimespan")
		m._showNextImageDirection% = 1
		m._ShowNextSlide(1, false)
	End Function ' NextImage
	
	obj.PreviousImage = Function() as Void
		m._currentSlideIndex% = m._currentSlideIndex% - 2
		m._QueueSlide(m._currentSlideIndex%)
		m._showNextImageGracePeriod = CreateObject("roTimespan")
		m._showNextImageDirection% = -1
		m._ShowNextSlide(-1, false)
	End Function ' PreviousImage
	
	obj.SetDisplayMode = Function(displayMode$) as Void
		m._displayMode$ = displayMode$
	End Function ' SetDisplayMode
	
	obj.IsTextOverlayVisible = Function() as Boolean
		Return m._showTextOverlay
	End Function ' IsTextOverlayVisible
	
	obj.SetTextOverlayVisible = Function(visible as Boolean) as Void
		m._showTextOverlay = visible
	End Function ' SetTextOverlayIsVisible
	
	obj.AddHeader = Function(name$, value$) as Void
		m._httpHeaders[name$] = value$
	End Function ' AddHeader
	
	' Should be called once every frame to update all logic of the component
	obj.UpdateLogic = Function() as Void
		If m._screen <> Invalid
			' Close if requested to do so
			If m._done
				' Clear the reference to the screen, which _should_ force it to close
				m._screen = Invalid
				
				' Delete all the temporary files
				m._fs.Delete("tmp:/ImprovedSlideshow/")
				
				Return
			End If
			
			TweenerUpdateTweens()
			
			' Load next image if it is prudent
			If m._showNextImageGracePeriod <> Invalid And m._showNextImageGracePeriod.TotalMilliseconds() >= 5
				m._LoadAndPositionCurrentImage(true, m._showNextImageDirection%)
				m._showNextImageGracePeriod = Invalid
			End If
			
			' Go to the next frame if the timespan has elapsed
			If m._playing And m._timespan.TotalMilliseconds() >= m._period! * 1000
				' Pretend we pressed the right arrow to prevent the screen saver from triggering
				' Certainly there's a better way to do this...?
				xfer = CreateObject("roURLTransfer")
				xfer.SetUrl("http://localhost:8060/keypress/Fwd")
				xfer.PostFromString("")
			End If
			
			' Delete all old slides that are no longer tweening
			For Each oldSlide in m._previousSlideQueue
				If Not TweenerIsTweening(oldSlide) Then m._previousSlideQueue.Shift()
			End For
		End If
	End Function ' UpdateLogic
	
	' Draws everything to the screen
	obj.Draw = Function() as Void
		If m._screen <> Invalid
			m._screen.Clear(&h00000000)
			
			' Show all old slides as long as they're tweening
			For Each oldSlide in m._previousSlideQueue
				oldSlide.Draw(m._screen)
			End For
			
			If m._currentSlideSprite <> Invalid 
				m._currentSlideSprite.Draw(m._screen)
			End If
			
			' Show play/pause buttons (will likely have alpha=0 most of the time)
			m._playSprite.Draw(m._screen)
			m._pauseSprite.Draw(m._screen)
			
			' Show the text overlay
			If m._showTextOverlay
				safeX% = m._screen.GetWidth() * 0.1
				startY% = m._screen.GetHeight() * 0.7
				
				' Draw a box around the location of the text
				em% = m._font.GetOneLineWidth("M", m._screen.GetWidth())
				rectX1% = safeX% - em%
				rectX2% = m._screen.GetWidth() - rectX1%
				rectY1% = startY% - 0.5 * em%
				rectY2% = m._screen.GetHeight() * 0.9
				draw = GetDrawUtil()
				draw.RectFilled(m._screen, rectX1%, rectY1%, rectX2%, rectY2%, &h3e5779cc)
				draw.RectOutline(m._screen, rectX1% - 1, rectY1% - 1, rectX2% + 1, rectY2% + 1, &hFFFFFFFF)
				draw.RectOutline(m._screen, rectX1% - 2, rectY1% - 2, rectX2% + 2, rectY2% + 2, &hFF)
				
				metadata = m._content[m._currentSlideIndex%]
				If metadata.TextOverlayUL <> Invalid 
					draw.TextWithShadow(m._screen, metadata.TextOverlayUL, safeX%, startY%, m._font)
				End If
				
				If metadata.TextOverlayUR <> Invalid 
					textWidth% = m._font.GetOneLineWidth(metadata.TextOverlayUR, m._screen.GetWidth() - 2 * safeX%)
					draw.TextWithShadow(m._screen, metadata.TextOverlayUR, m._screen.GetWidth() - textWidth% - safeX%, startY%, m._font)
				End If
				
				If metadata.TextOverlayBody <> Invalid
					' Split into wrapped lines only once if it hasn't already been done
					If metadata.TextOverlayBodyWrap = Invalid
						' First split into multiple lines based on newline (Chr(10)) characters
						explicitLines = metadata.TextOverlayBody.Tokenize(Chr(10))
						wrappedLines = []
						For Each line in explicitLines
							wrappedLines.Append(GetTextUtil().Wrap(line, m._font, m._screen.GetWidth() - 2 * safeX%))
						End For
						metadata.TextOverlayBodyWrap = wrappedLines
					End If
					
					textHeight% = m._font.GetOneLineHeight()
					For i% = 0 to metadata.TextOverlayBodyWrap.Count() - 1
						draw.TextWithShadow(m._screen, metadata.TextOverlayBodyWrap[i%], safeX%, startY% + textHeight% * (1.2 + i%), m._font)
					End For
				End If
			End If
			
			m._screen.Finish()
			m._screen.SwapBuffers()
		End If
	End Function ' Draw
	
	obj.Show = Function() as Boolean
		' Create temp directory
		m._fs.CreateDirectory("tmp:/ImprovedSlideshow/")
	
		' roScreen objects are shown immediately after creation
		m._screen = CreateObject("roScreen", true)
		m._screen.SetAlphaEnable(true)
		m._screen.SetMessagePort(m._port)
		If m.Resume(false)
			m._QueueSlide(m._currentSlideIndex%)
			m._ShowNextSlide()
		End If
		
		m._done = False
	End Function ' Show
	
	obj.Close = Function() as Void
		m._done = True
	End Function ' Close
	
	obj.IsClosed = Function() as Boolean
		Return m._done
	End Function ' IsClosed
	
	' Shows the next slide, loading it first if necessary
	obj._ShowNextSlide = Function(direction% = 1, showImage = true) as Void
		idx% = m._GetNextSlideIndex(m._currentSlideIndex%)
		file$ = m._IndexToFilename(idx%)
		If m._fs.Exists(file$)
			m._currentSlideIndex% = idx%
			m._timespan.Mark()
			If showImage Then m._LoadAndPositionCurrentImage(true, direction%)
		Else
			? "Trying to show " + file$ + ", but it does not yet exist. Queuing..."
			If m._currentSlideSprite <> Invalid Then m._previousSlideQueue.Push(m._currentSlideSprite)
			m._currentSlideSprite = Invalid
			m._QueueSlide(idx%)
		End If
	End Function ' _ShowNextSlide
	
	obj._LoadAndPositionCurrentImage = Function(tweenIn = true as Boolean, direction% = 0) as Void
		m._showNextImageGracePeriod = Invalid
		idx% = m._currentSlideIndex%
		file$ = m._IndexToFilename(idx%)
		bmp = CreateObject("roBitmap", file$)
		If bmp <> Invalid
			? "Showing slide " + file$
			If m._currentSlideSprite <> Invalid
				oldSlide = m._currentSlideSprite
				m._previousSlideQueue.Push(oldSlide)
				NewTweener(oldSlide, GetEaseUtil().CubicInOut).Duration(0.65).X(oldSlide.GetX(), -direction% * m._screen.GetWidth() * 2).Start()
				' Reset zoom if necessary
				If m._currentZoomFactor! <> 1.0
					NewTweener(oldSlide, GetEaseUtil().CubicInOut).Duration(0.5).ScaleX(oldSlide.GetScaleX(), m._currentDefaultPosition.sx!).ScaleY(oldSlide.GetScaleY(), m._currentDefaultPosition.sy!).Y(oldSlide.GetY(), m._currentDefaultPosition.y%).Start()
				End If
			End If
			
			' Determine position of next slide on the screen
			m._currentZoomFactor! = 1.0
			details = m._GetScaleFactors(bmp)
			m._currentDefaultPosition = details
			m._currentSlideSprite = NewSprite(bmp, details.x%, details.y%, 1.0, details.sx!, details.sy!)
			If tweenIn Then NewTweener(m._currentSlideSprite, GetEaseUtil().CubicInOut).Duration(0.65).X(direction% * m._screen.GetWidth() * 2, details.x%).Start()
			
			' Start queuing previous and next slides to be safe on both ends
			idx2% = m._GetNextSlideIndex(idx%)
			If Not m._IsQueuedOrCached(idx2%) Then m._QueueSlide(idx2%)
			idx2% = m._GetPreviousSlideIndex(idx%)
			If Not m._IsQueuedOrCached(idx2%) Then m._QueueSlide(idx2%)
		Else 
			? "Error creating roBitmap from " + file$ + " => Showing black instead."
			If m._currentSlideSprite <> Invalid Then m._previousSlideQueue.Push(m._currentSlideSprite)
			m._currentSlideSprite = Invalid
		End If
	End Function ' _LoadAndPositionImage
	
	' Determines the position and scale factors for the input bitmap object
	' Returns an associative array with properties x%, y%, sx!, and sy! for
	' the x and y position, and x and y scale, respectively
	obj._GetScaleFactors = Function(bmp as Object) as Object
		sx! = m._screen.GetWidth() / bmp.GetWidth()
		sy! = m._screen.GetHeight() / bmp.GetHeight()
		
		If m._displayMode$ = "scale-to-fill"
			Return { x%: 0, y%: 0, sx!: sx!, sy!: sy! }
		Else
			If m._displayMode$ = "zoom-to-fill"
				s! = GetMath().Max(sx!, sy!)
			Else
				' Default to "scale-to-fit"
				s! = GetMath().Min(sx!, sy!)
			End If
			Return {x%: (m._screen.GetWidth() - bmp.GetWidth() * s!) / 2, y%: (m._screen.GetHeight() - bmp.GetHeight() * s!) / 2, sx!: s!, sy!: s! }
		End If
	End Function ' _GetScaleFactors
	
	obj._GetNextSlideIndex = Function(currentIndex%) as Integer
		idx% = currentIndex% + 1
		If idx% >= m._content.Count()
			Return 0
		Else If idx% < 0
			Return m._content.Count() - 1
		Else
			Return idx%
		End If
	End Function ' _GetNextSlideIndex
	
	obj._GetPreviousSlideIndex = Function(currentIndex%) as Integer
		idx% = currentIndex% - 1
		If idx% < 0
			Return m._content.Count() - 1
		Else
			Return idx%
		End If
	End Function ' _GetPreviousSlideIndex
	
	' Checks if a slide index is queued for download or cached
	obj._IsQueuedOrCached = Function(idx%) as Boolean
		If idx% < 0 Or idx% >= m._content.Count() Then Return False
		
		Return m._IsCached(idx%) Or m._cacheUrlTransferByIdx.DoesExist(Stri(idx%))
	End Function ' _IsQueuedOrCached
	
	obj._IsCached = Function(idx%) as Boolean
		Return m._fs.Exists(m._IndexToFilename(idx%))
	End Function ' _IsCached
	
	' Begins caching the slide with the provided index if it is not already being cached
	' idx% Index of the slide to queue downloading
	obj._QueueSlide = Function(idx%) as Void
		' Ignore nonsensical indices
		If idx% < 0 Or idx% >= m._content.Count() Then Return
		
		' Do we have the next slide cached yet?
		If Not m._IsQueuedOrCached(idx%)
			url$ = m._content[idx%].Url
			xfer = CreateObject("roUrlTransfer")
			xfer.SetUrl(url$)
			xfer.SetMessagePort(m._port)
			dest$ = m._IndexToFilename(idx%)
			xfer.SetHeaders(m._httpHeaders)
			xfer.AsyncGetToFile(dest$)
			xferData = {
				dest$: dest$
				xfer: xfer
				idx%: idx%
			}
			m._cacheUrlTransferById[Stri(xfer.GetIdentity())] = xferData
			m._cacheUrlTransferByIdx[Stri(idx%)] = xferData
			? "_QueueSlide(" + Stri(idx%).Trim() + "); url:" + url$ + "; destination: " + dest$
		Else
			? "_QueueSlide(" + Stri(idx%).Trim() + ") => Already queued or cached"
		End If
	End Function ' 
	
	obj._IndexToFilename = Function(idx%) as String
		' TODO: Need better handling of file type here!
		Return "tmp:/ImprovedSlideshow/" + Stri(idx%).Trim() + ".jpg"
	End Function ' _IndexToFilename
	
	obj.HandleMessage = Function(msg as Object) as Void
		If Type(msg) = "roUrlEvent"
			If msg.GetInt() = 1 
				xferData = m._cacheUrlTransferById[Stri(msg.GetSourceIdentity())]
				If xferData <> Invalid
					If msg.GetResponseCode() = 200
						' Transfer succeeded! Remove reference to xfer object and create a bitmap with the data
						? "Xfer succeeded to " + xferData.dest$
						m._cacheUrlTransferById.Delete(Stri(msg.GetSourceIdentity()))
						m._cacheUrlTransferByIdx.Delete(Stri(xferData.idx%))
						
						' Display immediately if this is the active slide
						If m._currentSlideIndex% = xferData.idx%
							Print "Displaying immediately"
							m._currentSlideIndex% = m._currentSlideIndex% - 1
							m._ShowNextSlide()
						End If
						
						' Queue next one immediately if in fetchAll mode
						If m._fetchAll
							m._QueueSlide(xferData.idx% + 1)
						End If
					Else
						? "Xfer failed to " + xferData.dest$ + ": " + msg.GetFailureReason()
					End If
				Else
					? "ERROR: Could not find xferData for id " + Stri(msg.GetSourceIdentity())
					If msg.GetResponseCode() = 200 Then ? "AND THE XFER WAS SUCCESSFUL, TOO!!"
				End If
			Else
				? "Unsupported roUrlEvent type: " + Stri(msg.GetInt()).Trim()
			End If
			
		Else If Type(msg) = "roUniversalControlEvent"
			action = msg.GetInt()
			If action = 0
				' Back pressed down
				m.Close()
			Else If action = 2
				' Up arrow
				m._ScrollUp()
			Else If action = 3
				' Down arrow
				m._ScrollDown()
			Else If action = 4
				' Left arrow
				m._ScrollLeft()
			Else If action = 5
				' Right arrow
				m._ScrollRight()
			Else If action = 6
				' Select pushed: reset zoom
				m._currentZoomFactor = 1.0
				m._ZoomTween(m._currentSlideSprite, 1.0)
			Else If action = 8
				' Rewind pressed down
				m.PreviousImage()
			Else If action = 9
				' Fast forward
				m.NextImage()
			Else If action = 10
				' Star button pressed, toggle overlay
				m.SetTextOverlayVisible(Not m.IsTextOverlayVisible())
			Else If action = 13
				' Play/Pause pushed
				If m._playing
					m.Pause()
				Else
					m.Resume()
				End If
			Else If action = 17
				' A pressed
				m._ZoomIn()
			Else If action = 18
				' B pressed
				m._ZoomOut()
			End If
		End If
	End Function ' HandleMessage
	
	obj._ZoomIn = Function()
		spr = m._currentSlideSprite
		If spr <> Invalid
			m._currentZoomFactor! = m._currentZoomFactor! * 2
			m._zoomTween(spr, m._currentZoomFactor!)
		End If
	End Function ' _ZoomIn
	
	obj._ZoomOut = Function()
		spr = m._currentSlideSprite
		If spr <> Invalid
			m._currentZoomFactor! = m._currentZoomFactor! / 2
			m._zoomTween(spr, m._currentZoomFactor!)
		End If
	End Function ' _ZoomOut
	
	obj._ZoomTween = Function(spr as Object, z!) as Void
		' Breaking this into multiple tweens for the sole purpose of code readability
		details = m._currentDefaultPosition
		NewTweener(spr, GetEaseUtil().CubicInOut).Duration(0.35).ScaleX(spr.GetScaleX(), details.sx! * z!).ScaleY(spr.GetScaleY(), details.sy! * z!).Start()
		NewTweener(spr, GetEaseUtil().CubicInOut).Duration(0.35).X(spr.GetX(), (m._screen.GetWidth() - spr.GetRegion().GetWidth() * details.sx! * z!) / 2).Start()
		NewTweener(spr, GetEaseUtil().CubicInOut).Duration(0.35).Y(spr.GetY(), (m._screen.GetHeight()- spr.GetRegion().GetHeight()* details.sy! * z!) / 2).Start()
	End Function ' _ZoomTween
	
	obj._ScrollUp = Function() as Void
		m._ScrollTween(m._currentSlideSprite, 0, m._screen.GetHeight() * 0.2)
	End Function ' _ScrollUp
	
	obj._ScrollDown = Function() as Void
		m._ScrollTween(m._currentSlideSprite, 0, -m._screen.GetHeight() * 0.2)
	End Function ' _ScrollDown
	
	obj._ScrollLeft = Function() as Void
		m._ScrollTween(m._currentSlideSprite, m._screen.GetWidth() * 0.25, 0)
	End Function ' _ScrollLeft
	
	obj._ScrollRight = Function() as Void
		m._ScrollTween(m._currentSlideSprite, -m._screen.GetWidth() * 0.25, 0)
	End Function ' _ScrollRight
	
	obj._ScrollTween = Function(spr as Object, x!, y!) as Void
		If spr <> Invalid Then NewTweener(spr, GetEaseUtil().CubicInOut).Duration(0.25).X(spr.GetX(), spr.GetX() + x!).Y(spr.GetY(), spr.GetY() + y!).Start()
	End Function ' _ScrollTween
	
	Return obj
End Function ' NewImprovedSlideshow
