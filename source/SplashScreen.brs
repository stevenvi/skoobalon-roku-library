' +---------------------------------------------------------------------------+
' | Skoobalon Roku Library                                                    |
' | Copyright (C) 2014 Steven T. Wallace                                      |
' |                                                                           |
' | This program is free software; you can redistribute it and/or modify      |
' | it under the terms of the GNU General Public License as published by      |
' | the Free Software Foundation; version 2 of the License.                   |
' |                                                                           |
' | This program is distributed in the hope that it will be useful,           |
' | but WITHOUT ANY WARRANTY; without even the implied warranty of            |
' | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
' | GNU General Public License for more details.                              |
' |                                                                           |
' | You should have received a copy of the GNU General Public License along   |
' | with this program; if not, write to the Free Software Foundation, Inc.,   |
' | 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.               |
' +---------------------------------------------------------------------------+


'
' Fades in a splash screen, waits a preset duration of time, then fades it back 
' out. This is a little nicer than the default splash screen behavior of the 
' Roku in that it allows the programmer to smoothly transition into their app 
' without any undesirable flickers.
'
' The ideal strategy for using this is to set the Roku splash screen to be a 
' single black pixel, and have the first action of your application be to 
' create a placeholder screen followed by showing this splash screen. (The 
' placeholder screen is required because the Roku will close the program at the
' end of a function where the last screen is invalidated, which happens with 
' the splash screen otherwise.) It will leave the screen black as the app is 
' loading, which for most small apps is instantaneous.
'
' See examples/SplashScreen for more details on how to use this component.
'
'
' @param image$ Absolute path to the splash image, such as "pkg:/SplashScreen.jpg"
' @param waitTime! Time to show the splash screen after having faded in before fading back out
' @param fadeTime! Time to fade in and out the splash screen
' @param easingFunction Easing function to use for the fade in/out transitions. See the Math object for more details
'
Function ShowSplashScreen(image$, waitTime! = 0.9, fadeTime! = 0.75, easingFunction = GetEaseUtil().CubicInOut as Object)
	? "Displaying splash screen for a total of"; (waitTime! + fadeTime! * 2); "seconds"
	bmp = CreateObject("roBitmap", image$)
	screen = CreateObject("roScreen", true)
	
	' Scale to fill the screen and center, if necessary
	sx! = screen.GetWidth() / bmp.GetWidth()
	sy! = screen.GetHeight() / bmp.GetHeight()
	ss! = GetMath().Min(sx!, sy!)
	x% = (screen.GetWidth() - bmp.GetWidth() * ss!) / 2
	y% = (screen.GetHeight() - bmp.GetHeight() * ss!) / 2
	sprite = NewSprite(bmp, x%, y%, 1.0, ss!, ss!)
	
	? "Fading in..."
	tween = NewTweener(sprite, easingFunction).Duration(fadeTime!).Alpha(0.0, 1.0).Start()
	While Not tween.IsDone()
		TweenerUpdateTweens()
		screen.Clear(&h00000000)
		sprite.Draw(screen)
		screen.SwapBuffers()
	End While
	
	? "Waiting..."
	Sleep(1000 * waitTime!)
	
	? "Fading out..."
	tween = NewTweener(sprite, easingFunction).Duration(fadeTime!).Alpha(1.0, 0.0).Start()
	While Not tween.IsDone()
		TweenerUpdateTweens()
		screen.Clear(&h00000000)
		sprite.Draw(screen)
		screen.SwapBuffers()
	End While

	? "Splash screen complete"
End Function ' ShowSplashScreen
